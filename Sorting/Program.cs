﻿using System;

namespace Sorting
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] Sequance = { 5, 48, 8, 6, 46, 54, 13, 214, 85 };

            Console.WriteLine("BubbleSort");
            foreach (var i in BubbleSort(Sequance))
                Console.Write(i + " ");
            Console.WriteLine('\n');

            Console.WriteLine("InsertionSort");
            foreach (var i in InsertionSort(Sequance))
                Console.Write(i + " ");
            Console.WriteLine('\n');

            Console.WriteLine("MergeSort");
            foreach (var i in MergeSort(Sequance))
                Console.Write(i + " ");
            Console.WriteLine('\n');
        }

        public static int[] BubbleSort(int[] sequanceToBeSorted)
        {
            if (sequanceToBeSorted.Length > 0)
            {
                int[] sequance = (int[])sequanceToBeSorted.Clone();
                for (int i = 0; i < sequance.Length; i++)
                    for (int j = 0; j < sequance.Length - 1; j++)
                        if (sequance[j] < sequance[j + 1])
                        {
                            int temp = sequance[j];
                            sequance[j] = sequance[j + 1];
                            sequance[j + 1] = temp;
                        }
                return sequance;
            }
            return sequanceToBeSorted;
        }

        public static int[] InsertionSort(int[] sequanceToBeSorted)
        {
            if (sequanceToBeSorted.Length > 0)
            {
                int[] sequance = (int[])sequanceToBeSorted.Clone();
                for (int i = 0; i < sequance.Length - 1; i++)
                    for (int j = i + 1; j < sequance.Length; j++)
                        if (sequance[j] < sequance[i])
                        {
                            int temp = sequance[i];
                            sequance[i] = sequance[j];
                            sequance[j] = temp;
                        }
                return sequance;
            }
            return sequanceToBeSorted;
        }

        public static int[] Partition(int[] sequance, int start, int end)
        {
            int[] part = new int[end - start];
            int partIndex = 0;
            for (int i = start; i < end; i++)
                part[partIndex++] = sequance[i];
            return part;
        }
        public static int[] MergeSort(int[] sequanceToBeSorted)
        {
            if (sequanceToBeSorted.Length < 2)
                return sequanceToBeSorted;
            int middle = sequanceToBeSorted.Length / 2;
            int[] left = Partition(sequanceToBeSorted, 0, middle);
            int[] right = Partition(sequanceToBeSorted, middle, sequanceToBeSorted.Length);
            MergeSort(left);
            MergeSort(right);
            return Merge(left, right, sequanceToBeSorted);
        }
        public static int[] Merge(int[] left, int[] right, int[] sequance)
        {
            int leftIndex = 0;
            int rightIndex = 0;
            int initIndex = 0;
            while (leftIndex < left.Length && rightIndex < right.Length)
            {
                if (left[leftIndex] > right[rightIndex]) { sequance[initIndex] = left[leftIndex]; leftIndex++; }
                else { sequance[initIndex] = right[rightIndex]; rightIndex++; }
                initIndex++;
            }
            while (leftIndex < left.Length)
            {
                sequance[initIndex] = left[leftIndex];
                leftIndex++;
                initIndex++;
            }
            while (rightIndex < right.Length)
            {
                sequance[initIndex] = right[rightIndex];
                rightIndex++;
                initIndex++;
            }
            return sequance;
        }
    }
}
